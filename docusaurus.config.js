// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Lettres et langues anciennes - Académie de Montpellier',
  tagline: 'Ressources pédagogiques et informations utiles',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://lettres-montpellier.forge.apps.education.fr',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  // organizationName: 'DRANE ac-clermont', // Usually your GitHub org/user name.
  // projectName: 'doc-docusaurus', // Usually your repo name.

  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          //editUrl:
          //  'https://forge.apps.education.fr/fdelaruelle/labol/',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'Accueil',
        logo: {
          alt: 'Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Ressources pédagogiques',
          },
          {to: '/informations', label: 'Informations utiles', position: 'left'},

          //{
          //  href: 'https://forge.apps.education.fr/',
          //  label: 'Forge.apps',
          //  position: 'right',
          //},
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Menu',
            items: [
              {
                label: 'Ressources pédagogiques',
                to: '/docs/intro',
              },
              {
                label: 'Informations utiles',
                to: '/informations',
              },
            ],
          },
          {
            title: 'Autres services',
            items: [
              {
                label: 'Académie de Montpellier',
                href: 'https://www.ac-montpellier.fr/',
              },
              {
                label: 'Portail pédagogique Lettres et langues anciennes - Académie de Montpellier',
                href: 'https://pedagogie.ac-montpellier.fr/discipline/lettres',
              },
              //{
              //  label: 'Docusaurus',
              //  href: 'https://docusaurus.io/fr/',
              //},
            ],
          },
          //{
          //  title: 'Plus',
          //  items: [
          //                 {
          //      label: 'GitLab forge.apps',
          //      href: 'https://forge.apps.education.fr/',
          //    },
          //  ],
          //},
        ],
        //copyright: `Copyright © ${new Date().getFullYear()} construit avec Docusaurus.`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),
};

export default config;
