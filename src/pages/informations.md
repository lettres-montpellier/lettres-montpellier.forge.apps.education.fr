---
title: Page d'information principale
---

# Informations générales

## Interlocteurs en Lettres

* IPR1
* IPR2
* etc.
* Interlocteur académique pour le numérique en Lettres : Florian Delaruelle (florian.delaruelle@ac-montpellier.fr)
* Autres interlocuteurs



## Les différentes cercles d'étude de Lettres

### Le LaboL
Ce cercle d'étude a comme fonction de proposer des formations et des ressources en lien avec le numérique dans l'enseignement des Lettres.


### Autre cercle



### Autre cercle


## Ressources pédagogiques en ligne

### Édubase

[Lien vers le site](https://edubase.eduscol.education.fr/)

**Description :**

> Édubase s’adresse principalement aux enseignants, formateurs et inspecteurs du premier et du second degré.
> 
> Cette plateforme permet de rechercher un scénario pédagogique élaboré en académie en lien avec les programmes ainsi qu'avec le numérique éducatif et d'afficher sa description pour 20 disciplines et enseignements du premier degré au post-bac.

### Les lettres ÉduNum Lettres

[Lien vers le site](https://eduscol.education.fr/2629/lettre-edunum-lettres)

**Description :**

> Les lettres ÉduNum Lettres sont réalisées par les experts disciplinaires de la direction du numérique pour l’éducation, en collaboration avec l’inspection générale de l'éducation, du sport et de la recherche et les interlocuteurs académiques pour le numérique.
> 
> Mettant en avant des pratiques numériques proposées en académies par les groupes de réflexion disciplinaires, elles informent sur de nouvelles ressources numériques pour la classe ou utiles à la formation des enseignants.

### Le Portail pédagogique académique de Lettres

[Lien vers le site](https://pedagogie.ac-montpellier.fr/discipline/lettres)

**Description :**

> Le portail pédagogique académique permet de centraliser et de regrouper sous une même charte graphique toutes les ressources pédagogiques qui sont actuellement réparties de manière hétérogène sur le site académique, les sites disciplinaires académiques, l’ancien ENT et divers sites du 1er degré. Ces anciens sites sont appelés à disparaitre après transfert du contenu pertinent.

### Les banques de ressources numériques éducatives (BRNE)

[Lien vers la BRNE Langues et cultures de l'Antiquité](https://eduscol.education.fr/228/brne#summary-item-4)

**Description :**

> Les banques de ressources numériques éducatives (BRNE) offrent aux professeurs et aux élèves des ressources (contenus et outils) pour permettre des apprentissages via la création d'activités et de supports numériques pour la classe, la réalisation de cours, de situations d'entraînement, de révision, le suivi et l'évaluation des compétences travaillées.
