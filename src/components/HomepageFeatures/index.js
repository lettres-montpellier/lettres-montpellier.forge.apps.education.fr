import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Des ressources pédagogiques',
    Svg: require('@site/static/img/001.svg').default,
    description: (
      <>
        Vous pouvez trouver sur ce site des ressources pédagogiques directement utilisables pour les Lettres et les langues anciennes.
      </>
    ),
  },
  {
    title: 'Des informations utiles',
    Svg: require('@site/static/img/002.svg').default,
    description: (
      <>
       Vous pouvez également consulter sur ce site des informations diverses, en lien notamment avec le numérique pédagogique en Lettres.
      </>
    ),
  },
  {
    title: 'Contacts',
    Svg: require('@site/static/img/003.svg').default,
    description: (
      <>
       Pour toute question, contactez Florian Delaruelle, I.A.N. de Lettres, à 
       l'adresse suivante : florian.delaruelle@ac-montpellier.fr
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
