---
sidebar_position: 1
---
# Accueil

## Organisation

Grâce à la colonne de gauche, vous pouvez naviger entre différentes catégories de ressources.

### Ressources de l'Inspection de Lettres
Ce sont des ressources organisées et tirées de l'espace Pearltrees de l'Inspection de Lettres de Montpellier

### Les tutos du Labo
Ce sont des petits documents créés par les collègues du cercle d'études Lettres et numérique, et conçus pour faciliter l'usage du numérique dans l'enseignement des Lettres.


